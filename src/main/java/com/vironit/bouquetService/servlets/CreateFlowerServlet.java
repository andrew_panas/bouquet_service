package com.vironit.bouquetService.servlets;

import com.vironit.bouquetService.dao.BouquetDAO;
import com.vironit.bouquetService.dao.FlowerDAO;
import com.vironit.bouquetService.entities.Bouquet;
import com.vironit.bouquetService.entities.Flower;
import com.vironit.bouquetService.entities.enums.FlowerColor;
import com.vironit.bouquetService.entities.enums.FlowerName;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = { "/createFlower" })

public class CreateFlowerServlet extends HttpServlet {

    public CreateFlowerServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/createFlowerView.jsp");
        dispatcher.forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Bouquet bouquet = null;

        String name = request.getParameter("name");
        short length = Short.parseShort(request.getParameter("length"));
        String color = request.getParameter("color");
        float price = Float.parseFloat(request.getParameter("price"));
        BouquetDAO bouquetDAO = new BouquetDAO();
        try {
            if(!request.getParameter("bouquetId").equals(""))
                bouquet = bouquetDAO.getById(Long.parseLong(request.getParameter("bouquetId")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Flower flower = new Flower();
        flower.setName(FlowerName.valueOf(name));
        flower.setLength(length);
        flower.setColor(FlowerColor.valueOf(color));
        flower.setPrice(price);

        if(bouquet != null)
            flower.setBouquet(bouquet);
        else flower.setBouquet(null);

        try {
            FlowerDAO flowerDAO = new FlowerDAO();
            flowerDAO.add(flower);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.setAttribute("flower", flower);

        response.sendRedirect(request.getContextPath() + "/flowerList");

    }

}
