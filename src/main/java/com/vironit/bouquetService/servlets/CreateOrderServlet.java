package com.vironit.bouquetService.servlets;

import com.vironit.bouquetService.dao.OrderDAO;
import com.vironit.bouquetService.dao.UserDAO;
import com.vironit.bouquetService.entities.Order;
import com.vironit.bouquetService.entities.User;
import com.vironit.bouquetService.entities.enums.OrderStatus;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns = { "/createOrder" })
public class CreateOrderServlet extends HttpServlet {

    public CreateOrderServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/createOrderView.jsp");
        dispatcher.forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        User user = null;

        String address = request.getParameter("address");
        String phoneNumber = request.getParameter("phoneNumber");
        String status = request.getParameter("status");
        UserDAO userDAO = new UserDAO();
        try {
            user = userDAO.getById(Long.parseLong(request.getParameter("userId")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String errorString = null;

        Order order = new Order();
        order.setAddress(address);
        order.setPhoneNumber(phoneNumber);
        order.setStatus(OrderStatus.valueOf(status));
        order.setUser(user);

        try {
            OrderDAO orderDAO = new OrderDAO();
            orderDAO.add(order);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }

        request.setAttribute("order", order);

        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/createOrderView.jsp");
            dispatcher.forward(request, response);
        }
        else {
            response.sendRedirect(request.getContextPath() + "/orderList");
        }
    }

}