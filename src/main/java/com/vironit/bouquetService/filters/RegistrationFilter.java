package com.vironit.bouquetService.filters;

import com.vironit.bouquetService.dao.UserDAO;
import com.vironit.bouquetService.entities.User;
import com.vironit.bouquetService.entities.enums.Role;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebFilter(filterName = "registrationFilter", urlPatterns = { "/doRegistration" })


public class RegistrationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        UserDAO userDAO = new UserDAO();

        try {
            if (userDAO.isExistByEmail(email)) {
                request.getRequestDispatcher("/WEB-INF/views/registrationErrorView.jsp").forward(request, response);
            } else {
                String errorString = null;
                User user = new User();
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setPassword(password);
                user.setUserRole(Role.valueOf(Role.USER.toString()));
                try {
                    userDAO.add(user);
                } catch (SQLException e) {
                    e.printStackTrace();
                    errorString = e.getMessage();
                }
                request.setAttribute("errorString", errorString);
                request.setAttribute("user", user);
                response.sendRedirect(request.getContextPath() + "/login");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void destroy() {

    }
}
