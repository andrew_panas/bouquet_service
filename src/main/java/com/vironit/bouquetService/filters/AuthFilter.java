package com.vironit.bouquetService.filters;

import com.vironit.bouquetService.dao.UserDAO;
import com.vironit.bouquetService.entities.User;
import com.vironit.bouquetService.entities.enums.Role;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebFilter(filterName = "authFilter", urlPatterns = { "/login" })


public class AuthFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        final String email = request.getParameter("email");
        final String password = request.getParameter("password");

        UserDAO userDAO = new UserDAO();
        User user = null;

        final HttpSession session = request.getSession();

        if (session.getAttribute("email") != null && session.getAttribute("password") != null) {
            final String role = (String) session.getAttribute("role");
            moveToMenu(request, response, role);

        } else {
            try {
                if (userDAO.isExistByEmailPassword(email, password)) {

                    try {
                        user = userDAO.getByEmailPassword(email, password);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    String role = "";

                    request.getSession().setAttribute("password", password);
                    request.getSession().setAttribute("email", email);
                    if (user != null) {
                        request.getSession().setAttribute("role", role = user.getUserRole().toString());
                    }

                    moveToMenu(request, response, role);

                } else {

                    request.getRequestDispatcher("/WEB-INF/views/loginView.jsp").forward(request, response);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }


    private void moveToMenu(HttpServletRequest request, HttpServletResponse response, String role)
            throws ServletException, IOException {
        if (role.equals(Role.ADMIN.toString()))
            request.getRequestDispatcher("/WEB-INF/views/adminMenuView.jsp").forward(request, response);
        else if(role.equals(Role.USER.toString()))
            request.getRequestDispatcher("/WEB-INF/views/userMenuView.jsp").forward(request, response);
}


    @Override
    public void destroy() {

    }
}
