package com.vironit.bouquetService.entities.enums;

public enum FlowerName {
    ROZE,
    ASTER,
    PEONY,
    TULIP,
    CHAMOMILE,
    LILAC,
    CHRYSANTHEMUM
}
