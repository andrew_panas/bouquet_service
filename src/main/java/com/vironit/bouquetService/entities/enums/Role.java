package com.vironit.bouquetService.entities.enums;

public enum Role {
    USER,
    ADMIN
}
