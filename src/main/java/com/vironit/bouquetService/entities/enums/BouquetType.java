package com.vironit.bouquetService.entities.enums;

public enum BouquetType {
    ROUND,
    CASCADE,
    FLAME,
    JAPANESE,
    WEDDING
}