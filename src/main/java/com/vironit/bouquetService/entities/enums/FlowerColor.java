package com.vironit.bouquetService.entities.enums;

public enum FlowerColor {
    RED,
    YELLOW,
    BLUE,
    PINK,
    ORANGE,
    BROWN,
    WHITE,
    PURPLE,
    BLACK
}
