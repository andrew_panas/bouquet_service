package com.vironit.bouquetService.entities.enums;

public enum OrderStatus {
    NOT_CONFIRMED,
    CONFIRMED,
    IN_PROCESS,
    ASSEMBLED,
    DELIVERED
}
