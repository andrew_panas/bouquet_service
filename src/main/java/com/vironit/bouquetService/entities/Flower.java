package com.vironit.bouquetService.entities;

import com.vironit.bouquetService.entities.enums.FlowerColor;
import com.vironit.bouquetService.entities.enums.FlowerName;

public class Flower {
    private long id;
    private FlowerName name;
    private short length;
    private FlowerColor color;
    private float price;
    private Bouquet bouquet;


    public Flower(long id, FlowerName name, short length, FlowerColor color, float price, Bouquet bouquet) {
        this.id = id;
        this.name = name;
        this.length = length;
        this.color = color;
        this.price = price;
        this.bouquet = bouquet;
    }

    public Flower() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public FlowerName getName() {
        return name;
    }

    public void setName(FlowerName name) {
        this.name = name;
    }

    public short getLength() {
        return length;
    }

    public void setLength(short length) {
        this.length = length;
    }

    public FlowerColor getColor() {
        return color;
    }

    public void setColor(FlowerColor color) {
        this.color = color;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Bouquet getBouquet() {
        return bouquet;
    }

    public void setBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "id=" + id +
                ", name=" + name +
                ", length=" + length +
                ", color=" + color +
                ", price=" + price +
                ", bouquet=" + bouquet +
                '}';
    }
}