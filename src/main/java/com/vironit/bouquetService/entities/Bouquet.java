package com.vironit.bouquetService.entities;

import com.vironit.bouquetService.entities.enums.BouquetType;

import java.util.List;

public class Bouquet {
    private long id;
    private BouquetType type;
    private List<Flower> flowers;
    private Order order;



    public Bouquet(long id, BouquetType type, List<Flower> flowers, Order order) {
        this.id = id;
        this.type = type;
        this.flowers = flowers;
        this.order = order;
    }

    public Bouquet() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BouquetType getType() {
        return type;
    }

    public void setType(BouquetType type) {
        this.type = type;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "Bouquet{" +
                "id=" + id +
                ", type=" + type +
                ", flowers=" + flowers +
                ", order=" + order +
                '}';
    }
}
