package com.vironit.bouquetService.dao;

import com.vironit.bouquetService.dao.interfaces.UserDaoInterface;
import com.vironit.bouquetService.entities.User;
import com.vironit.bouquetService.entities.enums.Role;
import com.vironit.bouquetService.utils.ConnectionPool;
//import com.vironit.bouquetService.temp.DBConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements UserDaoInterface {

/*    private DBConnector dbConnector = new DBConnector();
    private Connection connection = null;*/

    @Override
    public void add(User user) throws SQLException {

        String sql = "INSERT INTO USERS(FIRST_NAME, LAST_NAME, EMAIL, PASSWORD, ROLE) VALUES (?, ?, ?, ?, ?::ROLE)";
        Connection connection = ConnectionPool.getInstance().getConnection();
//        connection = dbConnector.getConnection();

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getUserRole().toString());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.close();
        }
    }

    @Override
    public List<User> getAll() throws SQLException {
//        connection = dbConnector.getConnection();
        Connection connection = ConnectionPool.getInstance().getConnection();

        List<User> userList = new ArrayList<>();
        String sql = "SELECT USERS_ID, FIRST_NAME, LAST_NAME, EMAIL, PASSWORD, ROLE FROM USERS";

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getLong("USERS_ID"));
                user.setFirstName(resultSet.getString("FIRST_NAME"));
                user.setLastName(resultSet.getString("LAST_NAME"));
                user.setEmail(resultSet.getString("EMAIL"));
                user.setPassword(resultSet.getString("PASSWORD"));
                user.setUserRole(Role.valueOf(resultSet.getString("ROLE")));
                userList.add(user);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.close();
        }

        return userList;
    }

    @Override
    public User getById(long id) throws SQLException {

//        connection = dbConnector.getConnection();
        Connection connection = ConnectionPool.getInstance().getConnection();

        User user = new User();

        String sql = "SELECT USERS_ID, FIRST_NAME, LAST_NAME, EMAIL, PASSWORD, ROLE FROM USERS WHERE USERS_ID = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user.setId(resultSet.getLong("USERS_ID"));
                user.setFirstName(resultSet.getString("FIRST_NAME"));
                user.setLastName(resultSet.getString("LAST_NAME"));
                user.setEmail(resultSet.getString("EMAIL"));
                user.setPassword(resultSet.getString("PASSWORD"));
                user.setUserRole(Role.valueOf(resultSet.getString("ROLE")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.close();
        }

        return user;
    }

    @Override
    public void update(User user) throws SQLException {
//        connection = dbConnector.getConnection();
        Connection connection = ConnectionPool.getInstance().getConnection();

        String sql = "UPDATE USERS SET FIRST_NAME = ?, LAST_NAME = ?, EMAIL = ?, PASSWORD = ?, ROLE = ?::ROLE WHERE USERS_ID = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getUserRole().toString());
            preparedStatement.setLong(6, user.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.close();
        }
    }

    @Override
    public void remove(long id) throws SQLException {
//        connection = dbConnector.getConnection();
        Connection connection = ConnectionPool.getInstance().getConnection();

        String sql = "DELETE FROM USERS WHERE USERS_ID = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setLong(1, id);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.close();
        }
    }

    @Override
    public User getByEmailPassword(String email, String password) throws SQLException {

//        connection = dbConnector.getConnection();
        Connection connection = ConnectionPool.getInstance().getConnection();

        String sql = "SELECT USERS_ID, FIRST_NAME, LAST_NAME, EMAIL, PASSWORD, ROLE FROM USERS WHERE EMAIL = ? AND PASSWORD = ?";
        User user = new User();

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user.setId(resultSet.getLong("USERS_ID"));
                user.setFirstName(resultSet.getString("FIRST_NAME"));
                user.setLastName(resultSet.getString("LAST_NAME"));
                user.setEmail(resultSet.getString("EMAIL"));
                user.setPassword(resultSet.getString("PASSWORD"));
                user.setUserRole(Role.valueOf(resultSet.getString("ROLE")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.close();
        }

        return user;
    }


    @Override
    public User getByEmail(String email) throws SQLException {

//        connection = dbConnector.getConnection();
        Connection connection = ConnectionPool.getInstance().getConnection();

        String sql = "SELECT USERS_ID, FIRST_NAME, LAST_NAME, EMAIL, PASSWORD, ROLE FROM USERS WHERE EMAIL = ?";
        User user = new User();

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user.setId(resultSet.getLong("USERS_ID"));
                user.setFirstName(resultSet.getString("FIRST_NAME"));
                user.setLastName(resultSet.getString("LAST_NAME"));
                user.setEmail(resultSet.getString("EMAIL"));
                user.setPassword(resultSet.getString("PASSWORD"));
                user.setUserRole(Role.valueOf(resultSet.getString("ROLE")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.close();
        }

        return user;
    }


    @Override
    public boolean isExistByEmailPassword(String email, String password) throws SQLException {
        Connection connection = ConnectionPool.getInstance().getConnection();
//        connection = dbConnector.getConnection();

        String sql = "SELECT EMAIL, PASSWORD, ROLE FROM USERS WHERE EMAIL = ? AND PASSWORD = ?";
        User user = new User();
        boolean result = false;

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user.setEmail(resultSet.getString("EMAIL"));
                user.setPassword(resultSet.getString("PASSWORD"));
                user.setUserRole(Role.valueOf(resultSet.getString("ROLE")));
            }

            if(user.getEmail() != null && user.getPassword() != null)
                if(user.getEmail().equals(email) && user.getPassword().equals(password)){
                result = true;
                }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.close();
        }
        return result;
    }


    @Override
    public boolean isExistByEmail(String email) throws SQLException {
//        connection = dbConnector.getConnection();
        Connection connection = ConnectionPool.getInstance().getConnection();

        String sql = "SELECT EMAIL FROM USERS WHERE EMAIL = ?";
        User user = new User();
        boolean result = false;

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user.setEmail(resultSet.getString("EMAIL"));
            }

            if(user.getEmail() != null)
                if(user.getEmail().equals(email)){
                    result = true;
                }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.close();
        }
        return result;
    }
}
