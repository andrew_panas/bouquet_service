package com.vironit.bouquetService.dao.interfaces;

import com.vironit.bouquetService.entities.Order;

import java.sql.SQLException;
import java.util.List;

public interface OrderDaoInterface {

    void add(Order order) throws SQLException;

    List<Order> getAll() throws SQLException;

    Order getById(long id) throws SQLException;

    void update(Order order) throws SQLException;

    void remove(long id) throws SQLException;

}
