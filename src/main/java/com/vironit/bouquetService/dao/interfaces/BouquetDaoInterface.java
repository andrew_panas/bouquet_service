package com.vironit.bouquetService.dao.interfaces;

import com.vironit.bouquetService.entities.Bouquet;

import java.sql.SQLException;
import java.util.List;

public interface BouquetDaoInterface {

    void add(Bouquet bouquet);

    List<Bouquet> getAll() throws SQLException;

    Bouquet getById(long id) throws SQLException;

    void update(Bouquet bouquet) throws SQLException;

    void remove(long id) throws SQLException;

}
