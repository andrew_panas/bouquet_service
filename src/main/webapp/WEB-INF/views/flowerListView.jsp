<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="_menu.jsp"></jsp:include>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Список цветов</title>
</head>
<body>


<h3>Список цветов</h3>

<table border="1" cellpadding="5" cellspacing="1" >
    <tr>
        <th>ID</th>
        <th>Название</th>
        <th>Длина</th>
        <th>Цвет</th>
        <th>Цена</th>
        <th>ID Букета</th>
        <th>Редактировать</th>
        <th>Удалить</th>
    </tr>

    <c:forEach items="${flowerList}" var="flower" >

        <tr>
            <td>${flower.id}</td>

            <c:choose>
                <c:when test = "${flower.name == 'ROZE'}">
                    <td>Роза</td>
                </c:when>

                <c:when test = "${flower.name == 'ASTER'}">
                    <td>Астра</td>
                </c:when>

                <c:when test = "${flower.name == 'PEONY'}">
                    <td>Пиона</td>
                </c:when>

                <c:when test = "${flower.name == 'TULIP'}">
                    <td>Тюльпан</td>
                </c:when>

                <c:when test = "${flower.name == 'CHAMOMILE'}">
                    <td>Ромашка</td>
                </c:when>

                <c:when test = "${flower.name == 'LILAC'}">
                    <td>Лилия</td>
                </c:when>

                <c:when test = "${flower.name == 'CHRYSANTHEMUM'}">
                    <td>Хризантема</td>
                </c:when>
            </c:choose>

            <td>${flower.length}</td>

            <c:choose>
                <c:when test = "${flower.color == 'RED'}">
                    <td>Красный</td>
                </c:when>

                <c:when test = "${flower.color == 'YELLOW'}">
                    <td>Жёлтый</td>
                </c:when>

                <c:when test = "${flower.color == 'BLUE'}">
                    <td>Синий</td>
                </c:when>

                <c:when test = "${flower.color == 'PINK'}">
                    <td>Розовый</td>
                </c:when>

                <c:when test = "${flower.color == 'ORANGE'}">
                    <td>Оранжевый</td>
                </c:when>

                <c:when test = "${flower.color == 'BROWN'}">
                    <td>Коричневый</td>
                </c:when>

                <c:when test = "${flower.color == 'WHITE'}">
                    <td>Белый</td>
                </c:when>

                <c:when test = "${flower.color == 'PURPLE'}">
                    <td>Фиолетовый</td>
                </c:when>

                <c:when test = "${flower.color == 'BLACK'}">
                    <td>Чёрный</td>
                </c:when>
            </c:choose>

            <td>${flower.price}</td>
            <td>${flower.bouquet.id}</td>

            <td><a href="editFlower?id=${flower.id}">Редактировать</a></td>
            <td><a href="deleteFlower?id=${flower.id}">Удалить</a></td>
        </tr>
    </c:forEach>
</table>

<a href="createFlower" >Добавить цветок</a>


</body>
</html>